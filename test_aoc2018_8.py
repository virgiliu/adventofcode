import pytest

from .aoc2018_8 import Tree, Node


@pytest.fixture()
def num_list():
    return "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"


@pytest.fixture()
def tree(num_list):
    return Tree(num_list)


def test_tree(tree, num_list):
    assert tree.root == Tree(num_list).root


def test_tree_sum(tree):
    assert tree.sum == 138


@pytest.mark.parametrize("read_idx,expected", [
    (0, [2, 3]),
    (2, [0, 3]),
    (7, [1, 1]),
    (9, [0, 1])
])
def test_read_header(tree, read_idx, expected):
    assert Node(tree, read_idx).header == expected


@pytest.mark.parametrize("read_idx,expected", [
    (0, [1, 1, 2]),
    (2, [10, 11, 12]),
    (7, [2]),
    (9, [99])
])
def test_read_metadata(tree, read_idx, expected):
    assert Node(tree, read_idx).metadata == expected


@pytest.mark.parametrize("read_idx,expected", [
    (0, 138),
])
def test_node_sum(tree, read_idx, expected):
    assert Node(tree, read_idx).sum == expected


@pytest.mark.parametrize("read_idx,expected", [
    (0, 66),
    (2, 33),
])
def test_value(tree, read_idx, expected):
    assert Node(tree, read_idx).value == expected
