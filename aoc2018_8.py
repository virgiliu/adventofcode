"""Solution for day 8 of Advent of Code 2018 https://adventofcode.com/2018/day/8
The comments assume you are familiar with the problem details.
"""

from __future__ import annotations  # So that Node.__eq__ can use Node in type hinting

HEADER_LEN = 2
CHILD_COUNT_IDX = 0
METADATA_COUNT_IDX = 1


# Could probably do without this class, but it feels nicer to have a class named Tree somewhere when dealing with trees
class Tree:
    def __init__(self, num_list):
        # Convert string to list of ints
        self.num_list = [int(digit) for digit in num_list.split()]

        self.root = Node(tree=self)

    @property
    def sum(self):
        return self.root.sum

    @property
    def value(self):
        return self.root.value


class Node:
    @property
    def child_count(self) -> int:
        return self.header[CHILD_COUNT_IDX]

    @property
    def metadata_count(self) -> int:
        return self.header[METADATA_COUNT_IDX]

    @property
    def num_list(self):
        return self.tree.num_list

    def __init__(self, tree: Tree, read_idx: int = 0):
        """A node is responsible for storing and parsing data.
        It does this by updating a cursor that "moves over" the list of integers that define the tree.

        :param tree: The tree to which this node belongs. Used mostly for convenience.
        :param read_idx: The position of the reading index/cursor
                         This is needed so we know where the parent node finished reading.
        """

        self.read_idx = read_idx
        self.tree = tree
        self.original_read_idx = read_idx  # Used only for debugging when printing the node

        # Extract header data by reading the integer at current reading index position and the next one
        self.header = self.num_list[self.read_idx:self.read_idx + HEADER_LEN]

        # Move reading index after the header
        self.read_idx += HEADER_LEN

        # Extract children recursively, will update reading index value
        self.children = self.extract_children()

        # Extract metadata after extracting children, will update reading index value
        self.metadata = self.extract_metadata()

        self.value = self.compute_value()

    @property
    def sum(self) -> int:
        """Calculate the sum of this node on the fly."""
        # Could store it in a variable after init to avoid computing it each time we access this
        return sum(self.metadata) + sum([child.sum for child in self.children])

    def __repr__(self) -> str:
        return f'{self.original_read_idx}|{self.num_list}'

    def __eq__(self, other: Node):
        """Check if nodes are equal.
        If two nodes have the same original reading index and belong to same tree, they're the same node."""
        return self.tree.num_list == other.tree.num_list and self.original_read_idx == other.original_read_idx

    def extract_children(self):
        """Extract children from the tree's num_list based on current reading index and header info"""

        # Header says 0 children, return empty list
        if self.child_count == 0:
            return []

        children = []

        # Create one node for each child in the header
        for i in range(self.child_count):
            # Set the child node's reading index to be the current reading index so it can continue from this point
            child = Node(read_idx=self.read_idx, tree=self.tree)

            # After child was created and it created its own children (if any), it advanced the reading index.
            # We need to continue reading from where the child left off
            self.read_idx = child.read_idx

            # Don't forget to return the child
            children.append(child)

        # Release the children
        return children

    def extract_metadata(self) -> list:
        """Extracts metadata information from the num_list, using the self.read_idx cursor"""

        # No metadata, return empty list
        if self.metadata_count == 0:
            return []

        metadata = []

        # For each metadata entry in metadata_count, append the integer at the current reading index
        for i in range(self.metadata_count):
            metadata.append(self.num_list[self.read_idx])

            # Advance the reading index by 1
            self.read_idx += 1

        return metadata


    def compute_value(self) -> int:
        """Calculates the value of a node, for part 2 of the puzzle"""

        # No children, sum our metadata
        if self.child_count == 0:
            return sum(self.metadata)

        total = 0

        # Each metadata entry is used as an index for accessing the children
        for idx in self.metadata:
            # Metadata counts using 1-index
            idx -= 1

            # If index is valid, add the value of the child to this node's value
            if idx < self.child_count:
                total += self.children[idx].value

        return total


if __name__ == '__main__':
    with open('input', 'r') as file:
        tree_reader = Tree(file.read())
        print(f"Metadata sum: {tree_reader.sum}")
        print(f"Value: {tree_reader.value}")
