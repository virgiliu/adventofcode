Solution to [Advent of Code 2018 day 8](https://adventofcode.com/2018/day/8).

## Requirements
* Python 3.7.4+ (might work on older versions but I haven't tried)
* Optional: pytest (run `pip install pytest`, preferably in virtualenv)

## How to run
`python aoc2018_8.py`

It will read the contents of the `input` file and print the metadata sum
 and value for the puzzle.
 

TODO: Allow user to set input file path via argument


## How to run tests

Execute `pytest` in the repo root directory